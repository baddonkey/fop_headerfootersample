# FOP header and footer sample

Using Apache FOP to generate a PDF with multiple pages, header and footer processing the XML and Stylesheet.

```
fop -xml sample.xml -xsl sample.xsl -pdf output.pdf
```
