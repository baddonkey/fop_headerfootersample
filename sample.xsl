<xsl:stylesheet version="2.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <!-- Root template -->
    <xsl:template match="/document">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4">
                    <fo:region-body margin="1in"/>
                    <fo:region-before extent="1in"/>
                    <fo:region-after extent="0.5in"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="A4">
                <!-- Header -->
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block margin-top="1cm" margin-left="1in">
                        <fo:external-graphic src="url('logo.svg')" scaling="uniform" content-width="scale-to-fit" content-height="5mm" width="auto" height="5mm"/>
                        Swiss Castles - 01.01.2023
                    </fo:block>
                </fo:static-content>
                
                <!-- Footer -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        Page <fo:page-number/> of <fo:page-number-citation-last ref-id="last-page-marker"/>
                    </fo:block>
                </fo:static-content>

                <!-- Body content -->
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="page"/>
                    <!-- Bookmark for the last page -->
                    <fo:block id="last-page-marker"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <!-- Template for each page (castle) -->
    <xsl:template match="page">
        <fo:block>
            <xsl:value-of select="."/>
        </fo:block>
        <fo:block break-after="page"/>
    </xsl:template>

</xsl:stylesheet>
